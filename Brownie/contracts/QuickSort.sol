pragma solidity >=0.4.18 <0.9.0;
contract QuickSort {

    //contract data
    int[] private data_to_sort;
    //seed for getting random number
    uint private random = 0;

    function sort(int[] memory data) public payable {
        data_to_sort = data;
        if (data_to_sort.length > 1) {
            //internal function call
            quickSort(uint(0), uint(data_to_sort.length - 1));
        }
    }

    function quickSort(uint left, uint right) internal {
        if (left == right) {
            //recursion stop
            return;
        }

        uint from = left;
        uint to = right;
        //here I could use random partition instrad of fixed as below
        int pivot = data_to_sort[left + uint(randomGenerate(uint(right - left)))];
        //int pivot = data_to_sort[uint(left + (right - left) / 2)];
        

        //Hoare partition scheme
        while (from <= to) {
            while (data_to_sort[from] < pivot) {
                from++;
            }
            while (pivot < data_to_sort[to]) {
                to--;
            }
            if (from <= to) {
                (data_to_sort[from], data_to_sort[to]) = (data_to_sort[to], data_to_sort[from]);
                from++;
                if (to > 0) {
                    to--;
                }
            }
        }

        //recursive calls if needed
        if (left < to) {
            quickSort(left, to);
        }
            
        if (from < right) {
            quickSort(from, right);
        }
    }

    function viewResult() public view returns(int[] memory) {
        return data_to_sort;
    }

    function randomGenerate(uint mod) internal returns(uint) {
        // increase random seed
        random++;  
        return uint(keccak256(abi.encodePacked(block.timestamp, msg.sender, random))) % mod;
    }
}