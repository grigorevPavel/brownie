import pytest
from random import randint
from brownie import QuickSort, accounts

@pytest.fixture
def quick_sort():
    return QuickSort.deploy({'from': accounts[0]});

#@pytest.mark.timeout(100)
def test_alive(quick_sort):
    data = []
    quick_sort.sort(data, {'from': accounts[0]});
    res = quick_sort.viewResult({'from': accounts[0]});
    assert data == res

#@pytest.mark.timeout(500)
def test_obvious(quick_sort):
    for i in range(5):
        rand_num = randint(-999, 999)  
        data = [rand_num]
        quick_sort.sort(data, {'from': accounts[0]});
        res = quick_sort.viewResult({'from': accounts[0]});
        assert data == res
    

def test_harder(quick_sort):
    for i in range(50):
        data = [randint(-99999, 99999) for i in range(randint(2, 100))]
        quick_sort.sort(data, {'from': accounts[1]});
        assert sorted(data) == quick_sort.viewResult({'from': accounts[1]});


def test_crazy(quick_sort):
    data = [randint(-99999, 99999) for i in range(100)]
    sorted_data = sorted(data)
    quick_sort.sort(sorted_data, {'from': accounts[2]});
    assert sorted_data == quick_sort.viewResult({'from': accounts[2]});
